// Copyright (c) 2017 Uber Technologies, Inc.

package main

import (
	"gitlab.com/will.wang1/hotrod-driver/cmd"
)

func main() {
	cmd.Execute()
}
